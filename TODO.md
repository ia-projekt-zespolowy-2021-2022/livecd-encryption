Merge and push modified scripts to their original repositories:
- archiso-encrypted to https://gitlab.archlinux.org/mkinitcpio/mkinitcpio-archiso
  - path within repository: hooks/archiso
- mkarchiso to https://gitlab.archlinux.org/archlinux/archiso
  - path within repository: archiso/mkarchiso
  - requires changed or added profile (configs/...) according to build() function from our PKGBUILD
