# LiveCD encryption

This package provides a modified version of the mkarchiso script that allows the user to encrypt the created iso if the user so desires.
The script is installed in /usr/bin as mkarchiso-encrypted

Usage: Same as normal mkarchiso.
If you add the --encrypted or -e flag, it'll ask for a password and encrypt the file system before it's put in the iso. Upon launching the iso, you'll be prompted to enter the password.

It also adds a copy of the user's current baseline profile at the time of installation with some config files changed. The copy is called encryption and it is the profile that needs to be used for the script to work. 