# LiveCD encryption Authors

- Agata Margas \<agata.margas@student.uj.edu.pl\>
- Krzysztof Mrzigod \<krzysztof.mrzigod@student.uj.edu.pl\>
- Łukasz Klimek \<lukas.klimek@student.uj.edu.pl\>
- Oliwia Gil \<oliwia.gil@student.uj.edu.pl\>

**Project supervisor**
- Bartłomiej Bosek

# We used scripts from two other repositories:

## [Archiso](https://gitlab.archlinux.org/archlinux/archiso) Authors

- Aaron Griffin
- Adam Purkrt
- Alexander Epaneshnikov
- Chandan Singh
- Charles Vejnar
- Christian Hesse
- Christopher Brannon
- Dan McGee
- David Runge
- David Thurstenson
- Dieter Plaetinck
- Eli Schwartz
- Florian Pritz
- Francois Dupoux
- Gerardo Exequiel Pozzi
- Gerhard Brauer
- James Sitegen
- Justin Kromlinger
- Keshav Amburay
- Loui Chang
- Lukas Fleischer
- Martin Damian Fernandez
- Michael Vorburger
- Pierre Schmitz
- Sean Enck
- Simo Leone
- Steffen Bönigk
- Sven-Hendrik Haase
- Thomas Bächler
- Yu Li-Yu
- nl6720
- Øyvind Heggstad

## [Mkinitcpio-archiso](https://gitlab.archlinux.org/mkinitcpio/mkinitcpio-archiso) Authors

- Aaron Griffin
- Adam Purkrt
- Alexander Epaneshnikov
- Christian Hesse
- Dan McGee
- David Runge
- David Thurstenson
- Florian Pritz
- Francois Dupoux
- Gerardo Exequiel Pozzi
- Gerhard Brauer
- Loui Chang
- Lukas Fleischer
- Michael Vorburger
- Pierre Schmitz
- Simo Leone
- Simon Wilper
- Sven-Hendrik Haase
- Thomas Bächler
- nl6720
- Øyvind Heggstad
